#include<stdio.h>
#include<time.h>
int main()
{
    clock_t start, end;
    
    float elapsed_time;
    short src1[32]={3,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,9,8};
    short src2[32]={1,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,9,8};
    long dest[32]={0};
    
    int i;
    start = clock();
    for(i=0;i<32;i++)
    {
        dest[i]=src1[i]+src2[i];
    } 
    end = clock();
    elapsed_time = (double)(end - start)/(double)CLOCKS_PER_SEC;
    printf("Number of clocks : %lf Elapsed time: %lf seconds\n",(double)(end-start), elapsed_time);
    return 0;
}
