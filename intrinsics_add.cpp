#include<iostream>
#include<stdio.h>
#include <emmintrin.h>
int main()
{
    clock_t start, end;
    float elapsed_time;
    short src1[32]={1,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,9,8};
    short src2[32]={1,2,3,4,5,6,7,8,9,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,9,8};
    short dst[32]={0};
    
    int i=0,idx=0;
    __m128i vecSrc1, vecSrc2, vecDst;
    start = clock();
    for(i=0;i<4;i++,idx+=8)
    {
        vecSrc1 = _mm_loadu_si128 ((__m128i*)(src1+idx));
        vecSrc2 = _mm_loadu_si128 ((__m128i*)(src2+idx));
        vecDst = _mm_add_epi16 (vecSrc1, vecSrc2);
        _mm_storeu_si128 ((__m128i*)(dst+idx), vecDst);
    }
   
    end = clock();
    elapsed_time = (double)(end - start)/(double)CLOCKS_PER_SEC;
    printf("Number of clocks : %lf Elapsed time: %lf seconds\n",(double)(end-start), elapsed_time);
    return 0;
}
